from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout
import requests
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout

def index_view(request):

    if not request.user.is_authenticated:
        return render(request, 'login.html')


    try:
        URL = 'http://127.0.0.1/products/'
        r = requests.get(url = URL)
        data = r.json()
        products = data    
        return render(request, 'index.html', {'products':products})
    except:
        return render(request, 'index.html')

def auth(request):
    user = authenticate(username=request.POST.get('username',''), password=request.POST.get('pass',''))
    if user is not None:
        django_login(request, user)
        return redirect('/')
    else:
        return redirect('/')

def logout(request):
    django_logout(request)
    return redirect('/')

def product_view(request, id):
    URL = 'http://127.0.0.1/products/' + str(id)
    r = requests.get(url = URL)
    data = r.json()
    product = data

    return render(request, 'product.html', {'product':product})

def product_save(request, id):

    URL = 'http://127.0.0.1/products/' + str(id) + '/'
    r = requests.get(url = URL)
    data = r.json()

    print(data)
    
    stock = request.POST.get('stock')

    payload = {'id':id,'name':data['name'],'price':data['price'],'stock':stock}


    headers = {'Authorization':'Basic YWRtaW46MQ=='}
    r = requests.put("http://127.0.0.1/products/" + str(id) + "/", data=payload, headers=headers)

    print(r)
    return redirect('/')